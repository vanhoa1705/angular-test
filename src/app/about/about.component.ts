import { Component, OnDestroy, OnInit } from '@angular/core';
import { mergeMap } from 'rxjs/operators';
import { CommonService } from '../Services/common.service';
import { ServerHttpService } from '../Services/server-http.service';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss'],
})
export class AboutComponent implements OnInit, OnDestroy {
  public name = 'Nguyễn Văn Hoà';
  public age: number;
  public title = 'About';

  constructor(private common: CommonService) {
    this.age = common.age;
  }

  public tangTuoi() {
    this.common.age++;
    this.age = this.common.age;
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    console.log('destroy');
  }
}
