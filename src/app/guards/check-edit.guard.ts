import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanDeactivate,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { Observable } from 'rxjs';
import { PostDetailComponent } from '../post-detail/post-detail.component';

@Injectable({
  providedIn: 'root',
})
export class CheckEditGuard implements CanDeactivate<PostDetailComponent> {
  canDeactivate(
    component: PostDetailComponent,
    currentRoute: ActivatedRouteSnapshot,
    currentState: RouterStateSnapshot,
    nextState?: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    return component.editMode === -1 ? true : false;
  }
}
