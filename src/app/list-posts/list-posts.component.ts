import { Component, OnInit } from '@angular/core';
import { debounceTime, mergeMap } from 'rxjs/operators';
import { ServerHttpService } from '../Services/server-http.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.scss'],
})
export class ListPostsComponent implements OnInit {
  constructor(private serverHttp: ServerHttpService) {}

  public listPosts: any;
  public searchBox = new FormControl('');
  public title = 'Posts';

  ngOnInit(): void {
    this.serverHttp.getListPost().subscribe((data) => (this.listPosts = data));
    this.searchBox.valueChanges
      .pipe(debounceTime(600))
      .subscribe((key) =>
        this.serverHttp
          .searchPost(key)
          .subscribe((data) => (this.listPosts = data))
      );
  }
}
