import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { ServerHttpService } from '../Services/server-http.service';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.scss'],
})
export class PostDetailComponent implements OnInit {
  public post: any;
  public comments: any;
  public id: any;
  public editMode = -1;
  public body = new FormControl('');
  public updateCmt = new FormControl('');

  constructor(
    private serverHttp: ServerHttpService,
    private route: ActivatedRoute
  ) {}

  public update(id: number) {
    this.serverHttp.getComments(id).subscribe((data) => (this.comments = data));
  }

  public addComment(body: string, postId: number) {
    const comment = {
      author: 'Hoà Nguyễn',
      body: body,
      postId: postId,
    };
    this.serverHttp.addComment(comment).subscribe();
    this.body.reset();
    this.update(this.id);
  }

  public updateComment(postId: number, id: number, data: string) {
    const comment = {
      id: id,
      author: 'Hoà Nguyễn',
      body: data,
      postId: postId,
    };
    this.serverHttp.updateComment(comment).subscribe();
    this.editMode = -1;
    this.update(this.id);
  }

  public deleteComment(id: number) {
    this.serverHttp.deleteComment(id).subscribe();
    this.update(this.id);
  }

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
    this.serverHttp.getPost(this.id).subscribe((data) => (this.post = data));
    this.update(this.id);
  }
}
