import {
  Component,
  DoCheck,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { ServerHttpService } from '../Services/server-http.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit, OnChanges, DoCheck {
  constructor(private serverHttp: ServerHttpService) {}

  isAuth: boolean;
  @Input() title: string;

  public login = () => {
    this.serverHttp.login();
    this.isAuth = true;
  };

  public logout = () => {
    this.serverHttp.logout();
    this.isAuth = false;
  };

  public test = () => {};

  ngOnChanges(changes: SimpleChanges): void {
    console.log(changes);
  }

  ngOnInit(): void {
    this.isAuth = localStorage.isAuth === 'true';
  }

  ngDoCheck(): void {
    console.log('test DoCheck');
  }
}
