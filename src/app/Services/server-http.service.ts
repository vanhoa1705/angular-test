import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ServerHttpService {
  private httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };
  private REST_API_SERVER = 'http://localhost:3000';

  constructor(private httpClient: HttpClient, private router: Router) {}

  public getListPost() {
    const url = `${this.REST_API_SERVER}/posts`;
    return this.httpClient
      .get(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public test(data: any) {
    console.log(data);
    return of();
  }

  public login = () => {
    localStorage.isAuth = true;
  };

  public logout = () => {
    localStorage.isAuth = false;
    this.router.navigate(['/']);
  };

  public getPost(id: number) {
    const url = `${this.REST_API_SERVER}/posts/${id}`;
    return this.httpClient
      .get(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public searchPost(key: string) {
    const url = `${this.REST_API_SERVER}/posts?title_like=${key}`;
    return this.httpClient
      .get(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public getComments(postId: number) {
    const url = `${this.REST_API_SERVER}/posts/${postId}/comments`;
    return this.httpClient
      .get(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public getProfile() {
    const url = `${this.REST_API_SERVER}/profile`;
    return this.httpClient
      .get(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public addComment(data: any) {
    const url = `${this.REST_API_SERVER}/posts/${data.postId}/comments`;
    return this.httpClient
      .post(url, data, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public updateComment(data: any) {
    const url = `${this.REST_API_SERVER}/comments/${data.id}`;
    return this.httpClient
      .put(url, data, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  public deleteComment(id: number) {
    const url = `${this.REST_API_SERVER}/comments/${id}`;
    return this.httpClient
      .delete(url, this.httpOptions)
      .pipe(catchError(this.handleError));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` + `body was: ${error.error}`
      );
    }
    return throwError('Something bad happened; please try again later.');
  }
}
