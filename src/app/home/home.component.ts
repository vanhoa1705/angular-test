import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit {
  products = [
    {
      id: 0,
      name: 'PRODUCT ITEM NUMBER 1',
      description: 'Description for product item number 1',
      price: 2,
      quantity: 2,
    },
    {
      id: 1,
      name: 'PRODUCT ITEM NUMBER 2',
      description: 'Description for product item number 2',
      price: 3,
      quantity: 3,
    },
  ];

  public total: any;
  public title = 'Home';

  countCart() {
    this.total = {
      subTotal: 0,
      tax: 0,
      totalQuantity: 0,
      total: 0,
    };

    for (const product of this.products) {
      this.total.subTotal += product.price * product.quantity;
      this.total.totalQuantity += product.quantity;
    }
    this.total.tax = Math.round(0.1 * this.total.subTotal * 100) / 100;
    this.total.total = this.total.tax + this.total.subTotal;
  }

  updateQuantity(product: any, e: any) {
    let index = this.products.indexOf(product);
    this.products[index].quantity = parseInt(e.target.value);
    this.countCart();
  }

  removeProduct(product: any) {
    let index = this.products.indexOf(product);
    this.products.splice(index, 1);
    this.countCart();
  }

  ngOnInit(): void {
    this.countCart();
  }

  constructor() {}
}
